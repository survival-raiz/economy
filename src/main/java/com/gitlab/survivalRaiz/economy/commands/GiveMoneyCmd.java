package com.gitlab.survivalRaiz.economy.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import com.gitlab.survivalRaiz.permissions.management.Perms;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class GiveMoneyCmd extends ConfigCommand {

    private final EconomyPlugin plugin;

    public GiveMoneyCmd(EconomyPlugin economyPlugin) {
        super("transferir", "dá dinheiro ao jogador", "/transferir <jogador> <quantia>",
                new ArrayList<>(), "transferir");

        this.plugin = economyPlugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final OfflinePlayer op = Bukkit.getOfflinePlayer(strings[0]);

        if (!op.hasPlayedBefore())
            throw new SRCommandException(commandSender, Message.PLAYER_NOT_FOUND, plugin.getCore());

        double val;
        try {
            val = Double.parseDouble(strings[1]);
        } catch (NumberFormatException e) {
            throw new SRCommandException(commandSender, Message.SYNTAX, getUsage(), plugin.getCore());
        }

        if (!(commandSender instanceof Player)) return 0;

        final Player p = (Player) commandSender;

        if (Perms.SUPER_WALLET.allows(p)) return 0;

        if (plugin.getEconomy().has(p.getUniqueId(), val)) return 1;

        throw new SRCommandException(p, Message.PLAYER_BROKE, plugin.getCore());
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);
        final OfflinePlayer op = Bukkit.getOfflinePlayer(strings[0]);
        final double val = Double.parseDouble(strings[1]);

        plugin.getEconomy().depositPlayer(op.getUniqueId(), val);

        if (syntax == 1)
            plugin.getEconomy().withdrawPlayer(((Player) commandSender).getUniqueId(), val);

        // TODO: 12/26/20 Fix this when the new scoreboard API is implemented
        plugin.getCore().reloadScoreboard(op.getPlayer());
    }
}
