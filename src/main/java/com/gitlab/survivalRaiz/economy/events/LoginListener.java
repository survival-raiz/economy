package com.gitlab.survivalRaiz.economy.events;

import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class LoginListener implements Listener {

    private final EconomyPlugin plugin;

    public LoginListener(EconomyPlugin plugin) {
        this.plugin = plugin;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onFirstLogin(PlayerLoginEvent e) {
        if (!e.getPlayer().hasPlayedBefore())
            this.plugin.getEconomy().create(e.getPlayer().getUniqueId());

        // TODO: 12/26/20 Implementing here the logic for updating the scoreboard. To be deleted
        plugin.getServer().getOnlinePlayers().forEach(plugin.getCore()::reloadScoreboard);
    }
}
