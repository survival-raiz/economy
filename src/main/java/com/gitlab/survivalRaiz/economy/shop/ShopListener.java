package com.gitlab.survivalRaiz.economy.shop;

import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class ShopListener implements Listener {

    private final EconomyPlugin plugin;

    public ShopListener(EconomyPlugin plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onCloseShop(BlockBreakEvent e) {
        // TODO: 9/15/20 Simplificar isto, tá mt hardcoded

        final Location ref = e.getBlock().getLocation();
        if (e.getBlock().getType() == SignFormatConfig.SIGN_MATERIAL) {
            if (plugin.getCore().getDbManager().isShop(ref.getBlockX(), ref.getBlockY(), ref.getBlockZ()))
                plugin.getCore().getDbManager().remShop(ref.getBlockX(), ref.getBlockY(), ref.getBlockZ());
        } else if (e.getBlock().getType() == Material.CHEST) {
            final Location l1 = ref.clone(); l1.setX(ref.getBlockX() - 1);
            final Location l2 = ref.clone(); l2.setX(ref.getBlockX() + 1);
            final Location l3 = ref.clone(); l3.setZ(ref.getBlockZ() - 1);
            final Location l4 = ref.clone(); l4.setZ(ref.getBlockZ() + 1);

            if (l1.getBlock().getType() == SignFormatConfig.SIGN_MATERIAL)
                if (((WallSign) l1.getBlock().getState().getBlockData()).getFacing() == BlockFace.WEST)
                    if (plugin.getCore().getDbManager().isShop(l1.getBlockX(), l1.getBlockY(), l1.getBlockZ()))
                        plugin.getCore().getDbManager().remShop(l1.getBlockX(), l1.getBlockY(), l1.getBlockZ());

            if (l2.getBlock().getType() == SignFormatConfig.SIGN_MATERIAL)
                if (((WallSign) l2.getBlock().getState().getBlockData()).getFacing() == BlockFace.EAST)
                    if (plugin.getCore().getDbManager().isShop(l2.getBlockX(), l2.getBlockY(), l2.getBlockZ()))
                        plugin.getCore().getDbManager().remShop(l2.getBlockX(), l2.getBlockY(), l2.getBlockZ());

            if (l3.getBlock().getType() == SignFormatConfig.SIGN_MATERIAL)
                if (((WallSign) l3.getBlock().getState().getBlockData()).getFacing() == BlockFace.NORTH)
                    if (plugin.getCore().getDbManager().isShop(l3.getBlockX(), l3.getBlockY(), l3.getBlockZ()))
                        plugin.getCore().getDbManager().remShop(l3.getBlockX(), l3.getBlockY(), l3.getBlockZ());

            if (l4.getBlock().getType() == SignFormatConfig.SIGN_MATERIAL)
                if (((WallSign) l4.getBlock().getState().getBlockData()).getFacing() == BlockFace.SOUTH)
                    if (plugin.getCore().getDbManager().isShop(l4.getBlockX(), l4.getBlockY(), l4.getBlockZ()))
                        plugin.getCore().getDbManager().remShop(l4.getBlockX(), l4.getBlockY(), l4.getBlockZ());
        }
    }
}
