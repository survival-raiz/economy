package com.gitlab.survivalRaiz.economy.shop;

import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import com.gitlab.survivalRaiz.permissions.management.Perms;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class ShopManager {

    private final EconomyPlugin plugin;

    public ShopManager(EconomyPlugin plugin) {
        this.plugin = plugin;
    }

    public double getTax(ItemStack itemStack) {
        final double a = plugin.getEconomyConfig().getTaxBase();
        final double b = plugin.getEconomyConfig().getTaxMultiplier();

        final String hash = plugin.getEconomyConfig().hash(itemStack);
        final double x = plugin.getCore().getDbManager().getShopsForItem(hash);

        return a + b * x;
    }

    public double getShopCreationPrice(UUID player) {
        final double a = plugin.getEconomyConfig().getShopBase();
        final double b = plugin.getEconomyConfig().getShopMultiplier();

        final double x = plugin.getCore().getDbManager().getShopsFromPlayer(player);

        return a + b * (Math.exp(x));
    }

    /**
     * Creates a shop for the player in said location.
     *
     * @param owner the shop's owner
     * @param c     buy price for the item
     * @param v     sell price for the item
     * @param q     ammount of the item per purchase
     * @return 0 if successful, -1 if he is not looking at a chest, -2 if the chest is empty,
     * -3 if the chest has different types of items, -4 if the player has not enough money, -5 if the block where the
     * sign should be placed is null.
     */
    public int createShop(Player owner, double c, double v, int q) {
        /**/final long createShop_start = System.nanoTime();

//        https://www.spigotmc.org/threads/get-the-block-player-looking-at.234577/
        /**/final long checkTarget = System.nanoTime();
        final Block b = owner.getTargetBlock(null, 4);
        if (b.getType() != Material.CHEST)
            return -1;
        /**/System.out.println("Check target: " + (System.nanoTime() - checkTarget));

        final Chest chest = (Chest) b.getState();
        final Block place = getSignBlock(owner, chest.getLocation());

        if (place == null)
            return -5;

        /**/final long checkContents = System.nanoTime();
        final ItemStack[] contents = removeNull(chest.getInventory().getContents());

        if (contents.length == 0)
            return -2;

        ItemStack comp = contents[0];
        for (ItemStack content : contents)
            if (!comp.equals(content))
                return -3;
        /**/System.out.println("Check contents: " + (System.nanoTime() - checkContents));

        /**/final long getCreationPrice = System.nanoTime();
        final double price = getShopCreationPrice(owner.getUniqueId());
        if (!plugin.getEconomy().has(owner.getUniqueId(), price))
            return -4;

        plugin.getEconomy().withdrawPlayer(owner.getUniqueId(), price);
        /**/System.out.println("Get creation price: " + (System.nanoTime() - getCreationPrice));

        /**/final long createSign = System.nanoTime();
        place.setType(SignFormatConfig.SIGN_MATERIAL);
        final Sign s = createSign(owner, place);

        setupSign(s, comp, c, v, q, owner);
        /**/System.out.println("Create sign: " + (System.nanoTime() - createSign));

        /**/final long toDb = System.nanoTime();
        final String hash = plugin.getEconomyConfig().hash(comp);
        final Location location = s.getLocation();
        plugin.getCore().getDbManager().addShop(owner.getUniqueId(), hash, q, c, v,
                location.getBlockX(), location.getBlockY(), location.getBlockZ());
        /**/System.out.println("To DB: " + (System.nanoTime() - toDb));

        // TODO: 2/21/21 FIX THIS TO IMPLEMENT DYNAMIC PRICING
//        /**/final long updateSigns = System.nanoTime();
//        new BukkitRunnable() {
//            @Override
//            public void run() {
//                updateSigns(comp);
//            }
//        }.runTaskAsynchronously(plugin);
//        /**/System.out.println("Update all signs: " + (System.nanoTime() - updateSigns));

        /**/System.out.println("createShop: " + (System.nanoTime() - createShop_start));
        return 0;
    }

    private Sign createSign(Player owner, Block place) {
        final BlockFace signFace = owner.getFacing().getOppositeFace();

        place.setType(SignFormatConfig.SIGN_MATERIAL);

        final Sign sign = (Sign) place.getState();
        final WallSign wallSign = (WallSign) sign.getBlockData();

        wallSign.setFacing(signFace);
        sign.setBlockData(wallSign);

        sign.update();
        return sign;
    }

    private Block getSignBlock(Player reference, Location location) {
        final BlockFace signFace = reference.getFacing().getOppositeFace();

        switch (signFace) {
            case NORTH:
                location.setZ(location.getBlockZ() - 1);
                break;
            case SOUTH:
                location.setZ(location.getBlockZ() + 1);
                break;
            case WEST:
                location.setX(location.getBlockX() - 1);
                break;
            case EAST:
                location.setX(location.getBlockX() + 1);
                break;
            default:
                return null;
        }

        return Objects.requireNonNull(Bukkit.getWorld("world")).getBlockAt(location);
    }

    public ItemStack[] removeNull(Inventory inv) {
        return removeNull(inv.getContents());
    }

    private ItemStack[] removeNull(ItemStack[] contents) {
        final List<ItemStack> res = new ArrayList<>();

        for (ItemStack content : contents)
            if (content != null)
                res.add(content);

        return res.toArray(new ItemStack[0]);
    }

    private void setupSign(Sign s, ItemStack item, double c, double v, int q, Player owner) {
        // TODO: 2/21/21 FIX THE DYNAMIC ECONOMY SYSTEM
        
        for (int i = 1; i <= 4; i++) {
            final String format = plugin.getSignFormatConfig().getLineFormat(i);
//            final double tax = q * getTax(item);

            final String res = format.replaceAll("%c%", c /*+ tax*/ < 0 ? "Não" : String.valueOf(c /*+ tax*/))
                    .replaceAll("%v%", v /*- tax*/ < 0 ? "Não" : String.valueOf(v /*- tax*/))
                    .replaceAll("%q%", String.valueOf(q))
                    .replaceAll("%player%", Perms.ADMIN_SHOP.allows(owner) ? "SERVIDOR" : owner.getName())
                    .replaceAll("%item%", item.getType().name());

            s.setLine(i - 1, res);
            s.update();
        }
    }

    private void updateSigns(ItemStack item) {
        final String hash = plugin.getEconomyConfig().hash(item);
        final double tax = getTax(item);

        plugin.getCore().getDbManager().shopsOfItem(hash, resultSet -> {
            try {
                while (resultSet.next()) {
                    final double c = resultSet.getDouble(4);
                    final double v = resultSet.getDouble(5);
                    final int q = resultSet.getInt(3);
                    final double price = q * tax;

                    final Location l = new Location(Bukkit.getWorld("world"),
                            resultSet.getDouble(6), resultSet.getDouble(7),
                            resultSet.getDouble(8));

                    final Sign s = (Sign) Bukkit.getWorld("world").getBlockAt(l).getState();

                    final String owner = resultSet.getString(1);

                    setupSign(s, item, c + price, v - price, q,
                            Bukkit.getOfflinePlayer(UUID.fromString(owner)).getPlayer());
                }

                return null;
            } catch (SQLException var3) {
                return null;
            }
        });
    }
}