package com.gitlab.survivalRaiz.economy.shop;

import api.skwead.storage.file.yml.YMLConfig;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class SignFormatConfig extends YMLConfig {

    public static final Material SIGN_MATERIAL = Material.BIRCH_WALL_SIGN;

    public SignFormatConfig(JavaPlugin plugin) {
        super("sign_format", plugin);

        try {
            createConfig(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createConfig(Set<String> set) throws IOException {
        set = new HashSet<>();
        set.add("ln1");
        set.add("ln2");
        set.add("ln3");
        set.add("ln4");

        for (String param : set) {
            if (super.getConfig().get(param) == null) {
                super.getConfig().createSection(param);
                super.getConfig().set(param, "sample text");
                super.getConfig().save(super.getFile());
            }
        }
    }

    public String getLineFormat(int ln) {
        return getConfig().getString("ln" + ln);
    }
}
